// Source: https://www.geeksforgeeks.org/min-heap-in-java/
using System;

public class RandClass: IComparable
{
    public int i;

    public RandClass(int newI) {
        i = newI;
    }

    public int CompareTo(Object obj) {
        if (obj is RandClass) {
            RandClass other = obj as RandClass;
            if (i < other.i)
                return -1;
            else if (i > other.i)
                return 1;
            return 0;
        }
        return -2; // shouldn't compare different types
    }
}

public class MinHeap<T> where T: IComparable
{
    T[] heap;
    int size;
    int capacity;

    public MinHeap(int maxSize) {
        capacity = maxSize;
        size = 0;
        heap = new T[capacity];
    }

    int parent(int pos) {
        return (pos-1)/2;
    }

    int left(int pos) {
        return 2*pos+1;
    }

    int right(int pos) {
        return 2*pos+2;
    }

    void swap(int a, int b) {
        T temp = heap[a];
        heap[a] = heap[b];
        heap[b] = temp;
    }

    void minHeapify(int pos) {
        if (pos < size/2) { // if not leaf element
            if (heap[pos].CompareTo(heap[left(pos)]) > 0 ||
                    heap[pos].CompareTo(heap[right(pos)]) > 0) {
                if (heap[left(pos)].CompareTo(heap[right(pos)]) < 0) {
                    swap(pos, left(pos));
                    minHeapify(left(pos));
                }
                else {
                    swap(pos, right(pos));
                    minHeapify(right(pos));
                }
            }
        }
    }

    public bool insert(T val) {
        if (size >= capacity)
            return false;
        // insert at bottom, bubble up
        heap[size] = val;
        int curr = size;
        while (heap[curr].CompareTo(heap[parent(curr)]) < 0) {
            swap(curr, parent(curr));
            curr = parent(curr);
        }
        size++;
        return true;
    }

    public bool insert(T[] arr){
        if (arr.Length > capacity)
            return false;
        else {
            Array.Copy(arr, 0, heap, 0, arr.Length);
            size = arr.Length;
            sort();
            return true;
        }
    }

    public void sort() {
        for (int pos=size/2; pos>=0; pos--)
            minHeapify(pos);
    }

    public T remove() {
        if (size <= 0)
            return default(T);
        T minVal = heap[0];
        heap[0] = heap[--size];
        minHeapify(0);
        return minVal;
    }

    public void print() {
        int lineCount = 0;
        int lineLen = 1;
        for (int i=0; i<size; i++) {
            RandClass r = heap[i] as RandClass;
            Console.Write(r.i + " ");
            lineCount++;

            if (lineCount == lineLen) {
                lineCount = 0;
                lineLen *= 2;
                Console.WriteLine();

            }
        }
        Console.WriteLine();
    }
}

class MainClass {
    public static void Main (string[] args) {
        Console.WriteLine ("Hello World");

        MinHeap<RandClass> minHeap = new MinHeap<RandClass>(15);
        /*
           minHeap.insert(new RandClass(5));
           minHeap.insert(new RandClass(3)); 
           minHeap.insert(new RandClass(17));
           minHeap.insert(new RandClass(10)); 
           minHeap.insert(new RandClass(84)); 
           minHeap.insert(new RandClass(19));
           minHeap.insert(new RandClass(6));
           minHeap.insert(new RandClass(22)); 
           minHeap.insert(new RandClass(9));
           */

        RandClass[] randArr =  {
            new RandClass(19),
            new RandClass(5),
            new RandClass(3),
            new RandClass(6),
            new RandClass(84),
            new RandClass(10),
            new RandClass(22),
            new RandClass(17),
            new RandClass(9)
        };

        minHeap.insert(randArr);

        for (int i=0; i<10; i++) {
            Console.WriteLine();

            RandClass minVal = minHeap.remove();
            if (minVal == null)
                Console.WriteLine("Min val is null");
            else
                Console.WriteLine("The Min val is " + minVal.i);
        }
    }
}
